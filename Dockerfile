FROM python:3.8.6

WORKDIR /app

COPY requirements.txt .

RUN pip install -r requirements.txt

COPY . .

RUN groupadd -g 1000 g_abc_bank

RUN useradd -g g_abc_bank --uid 1000 u_abc_bank

RUN chown -R u_abc_bank:g_abc_bank /app

USER u_abc_bank

CMD ["gunicorn", "-c", "etc/gunicorn.conf.py", "wsgi"]