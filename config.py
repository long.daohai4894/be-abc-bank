import os

basedir = os.path.abspath(os.path.dirname(__file__))

# dotenv.load_dotenv(os.path.join(basedir, '../../.env'))

ETC_DIR = os.path.abspath(os.path.join(os.path.dirname(__file__), 'etc'))


class Config:
    SECRET_KEY = os.getenv('SECRET_KEY', 'my_precious_secret_key')
    LOGGING_CONFIG_FILE = os.path.join(ETC_DIR, 'logging.ini')
    ORIGIN_ALLOW_DOMAIN = os.getenv('ORIGIN_ALLOW_DOMAIN')
    API_BINDING_ADDRESS = os.getenv('API_BINDING_ADDRESS')
    URL_SCHEMA = os.getenv('URL_SCHEMA')
    MONGOALCHEMY_DATABASE = os.getenv('DB_NAME')
    MONGOALCHEMY_HOST = os.getenv('DB_HOST')
    DEBUG = False
    JWT_HEADER_TYPE = 'JWT'
    JWT_SECRET_KEY = os.getenv('JWT_SECRET_KEY')
    JWT_ACCESS_TOKEN_EXPIRES = 60 * 60 * 24 * 3  # expired in 3 days
