# coding=utf-8
# Reference: https://github.com/benoitc/gunicorn/blob/master/examples/example_config.py

from dotenv import load_dotenv, find_dotenv

load_dotenv(find_dotenv())

import os
import multiprocessing

from config import Config

_ROOT = os.path.abspath(os.path.join(
    os.path.dirname(__file__),
    '..',
))
_VAR = os.path.join(_ROOT, 'var')
_ETC = os.path.join(_ROOT, 'etc')

loglevel = 'info'
errorlog = "-"
accesslog = "-"

# bind = 'unix:%s' % os.path.join(_VAR, 'run/gunicorn.sock')
bind = os.getenv('API_BINDING_ADDRESS')
# workers = 3
workers = multiprocessing.cpu_count() * 2 + 1

timeout = 3 * 60  # 3 minutes
keepalive = 24 * 60 * 60  # 1 day

capture_output = True
