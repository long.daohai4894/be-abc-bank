import logging
import os
import dotenv
from src import create_app

__author__ = 'Dan'
_logger = logging.getLogger(__name__)

dotenv.load_dotenv(os.path.join(os.path.dirname(__file__), './.env'))
app = create_app('dev')

if __name__ == '__main__':
    app.run()
