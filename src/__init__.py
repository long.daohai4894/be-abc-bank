# coding=utf-8
import logging
from flask_cors import CORS
from flask import Flask

from src.helpers.login_manager import init_login_manager

_logger = logging.getLogger(__name__)


def create_app(config_name):
    import config
    import logging.config
    from src.resources.api_core import init_api
    from src.models import init_model
    from src.commands import init_command

    app = Flask(__name__)
    app.config.from_object(config.Config)
    app.config['MONGODB_SETTINGS'] = {
        'db': app.config['MONGOALCHEMY_DATABASE'],
        'host': app.config['MONGOALCHEMY_HOST'],
        'port': 27017
    }
    logging.config.fileConfig(app.config['LOGGING_CONFIG_FILE'], disable_existing_loggers=False)
    _logger.info("Starting in `{}` mode...".format(config_name))

    init_api(app)
    init_model(app)
    init_command(app)
    init_login_manager(app)
    app.config['CORS_HEADERS'] = 'Content-Type'
    CORS(app, resources={r"*": {"origins": app.config['ORIGIN_ALLOW_DOMAIN']}})
    return app
