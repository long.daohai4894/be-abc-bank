# coding=utf-8
import logging

from src.commands.fake_bank_account_command import fake_bank_account
from src.commands.fake_user_command import fake_user_account

_logger = logging.getLogger(__name__)


def init_command(app, **kwargs):
    """
    :param flask.Flask app: the app
    :param kwargs:
    :return:
    """

    app.cli.add_command(fake_bank_account)
    app.cli.add_command(fake_user_account)
