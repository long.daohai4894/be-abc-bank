# coding=utf-8
import random
import logging

from faker import Faker
from flask.cli import AppGroup

__author__ = 'LongDH'

from src.models import BankAccount

_logger = logging.getLogger(__name__)

faker = Faker()
CONTEXT_SETTINGS = dict(help_option_names=['-h', '--help'])
fake_bank_account = AppGroup('fake-bank-account', help="Command to fake data", context_settings=CONTEXT_SETTINGS)


@fake_bank_account.command('run', help='Run fake data')
def run():
    index, data = 1, []
    count_bank_acc = BankAccount.objects(account_number__lte=0).count()
    while len(data) <= 5000:
        data.append({
            'account_number': count_bank_acc + index,
            'balance': random.randint(1000, 100000),
            'firstname': faker.first_name(),
            'lastname': faker.last_name(),
            'age': random.randint(10, 70),
            'gender': random.choice(['F', 'M']),
            'address': faker.address(),
            'employer': faker.first_name(),
            'email': faker.email(),
            'city': faker.city(),
            'state': faker.state(),
        })
        index += 1

    bank_account_instances = [BankAccount(**item) for item in data]
    BankAccount.objects.insert(bank_account_instances, load_bulk=False)
