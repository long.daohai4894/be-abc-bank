# coding=utf-8
import random
import logging

from faker import Faker
from flask.cli import AppGroup

__author__ = 'LongDH'

from src.models import bcrypt
from src.models.user import User

_logger = logging.getLogger(__name__)

faker = Faker()
CONTEXT_SETTINGS = dict(help_option_names=['-h', '--help'])
fake_user_account = AppGroup('fake-user-account', help="Command to fake data", context_settings=CONTEXT_SETTINGS)


@fake_user_account.command('fake-admin', help='Run fake user data')
def run():
    user = User(**{
        'email': faker.email(),
        'name': faker.first_name(),
        'status': 'active',
        'password_hash': bcrypt.generate_password_hash('secret123').decode('utf-8'),
        'role': 'admin'
    })
    user.save()
    print('Success')


@fake_user_account.command('fake-normal', help='Run fake user data')
def run():
    user = User(**{
        'email': faker.email(),
        'name': faker.first_name(),
        'status': 'active',
        'password_hash': bcrypt.generate_password_hash('secret123').decode('utf-8'),
        'role': 'normal'
    })
    user.save()
    print('Success')