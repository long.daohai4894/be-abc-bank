from functools import wraps

from flask_jwt_extended import JWTManager, verify_jwt_in_request, get_jwt_identity
from werkzeug.exceptions import Forbidden

jwt = JWTManager()


def init_login_manager(app):
    jwt.init_app(app)


# Here is a custom decorator that verifies the JWT is present in
# the request, as well as insuring that this user has a role of
# `admin` in the access token
def admin_required(*roles):
    def wrapper(fn):
        @wraps(fn)
        def wrapper(*args, **kwargs):
            verify_jwt_in_request()
            current_user = get_jwt_identity()
            if current_user['role'] not in roles:
                raise Forbidden('User %s do not have a permission'
                                % current_user['email'])
            else:
                return fn(*args, **kwargs)

        return wrapper

    return wrapper
