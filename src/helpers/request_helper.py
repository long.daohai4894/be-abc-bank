from src.extensions.reqparse import RequestParser


class RequestHelper:
    @staticmethod
    def add_pagination_params(args=None):
        if not args:
            args = RequestParser(bundle_errors=True)
        args.add_argument('page', type=int, help='Page number, starting from 1',
                          required=False, default=1, location='args')
        args.add_argument('pageSize', type=int, help='Page size',
                          required=False, default=10, location='args')
        return args

    @staticmethod
    def filter_params(params: dict, pagination=False):
        args = RequestParser(bundle_errors=True)
        for param_name, param_type in params.items():
            args.add_argument(param_name, type=param_type, location='args')
        if pagination:
            return RequestHelper.add_pagination_params(args)
        return args

    @staticmethod
    def get_bank_account_list_filters():
        args = RequestHelper.add_pagination_params()
        args.add_argument('account_number', type=int, help='Account Number', location='args')
        args.add_argument('address', type=str, help='address', location='args')
        args.add_argument('age', type=int, help='age', location='args')
        args.add_argument('city', type=str, help='city', location='args')
        args.add_argument('email', type=str, help='email', location='args')
        args.add_argument('employer', type=str, help='employer', location='args')
        args.add_argument('firstname', type=str, help='firstname', location='args')
        args.add_argument('lastname', type=str, help='lastname', location='args')
        args.add_argument('gender', type=str, help='M | F', location='args')
        args.add_argument('state', type=str, help='state', location='args')
        args.add_argument('all_fields', type=str, help='all_fields', location='args')
        return args
