# coding=utf-8
import logging

_logger = logging.getLogger(__name__)


class ResponseHelper:
    @staticmethod
    def pagination_meta(page, page_size, total_items):
        """
        Pagination for creating metadata
        """
        total_pages = total_items // page_size if total_items % page_size == 0 \
            else (total_items // page_size) + 1
        next_page = page + 1 if page < total_pages - 1 else None
        previous_page = page - 1 if page > 1 else None
        return {
            'current_page': page,
            'page_size': page_size,
            'total_items': total_items,
            'next_page': next_page,
            'previous_page': previous_page,
            'total_pages': total_pages
        }

    @staticmethod
    def pagination(query, page, page_size):
        _pagination = query.paginate(page=page, per_page=page_size)
        total_items = _pagination.total
        items = _pagination.items
        return {'data': items,
                'metadata': ResponseHelper.pagination_meta(page, page_size, total_items)}