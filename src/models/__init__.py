import logging
import dotenv
import os

from flask_bcrypt import Bcrypt
from flask_mongoengine import MongoEngine

_logger = logging.getLogger(__name__)

basedir = os.path.abspath(os.path.dirname(__file__))
dotenv.load_dotenv(os.path.join(basedir, '../../.env'))

db = MongoEngine()
bcrypt = Bcrypt()


def init_model(app, **kwargs):
    """
    :param flask.Flask app: the app
    :param kwargs:
    :return:
    """
    _logger.info('MONGOALCHEMY_DATABASE_URI: %s' % app.config['MONGOALCHEMY_DATABASE'])
    db.app = app
    db.init_app(app)
    bcrypt.init_app(app)


from src.models.bank_account import BankAccount
