from src.models import db


class BankAccount(db.Document):
    account_number = db.IntField()
    address = db.StringField()
    age = db.IntField()
    balance = db.IntField()
    city = db.StringField(null=True)
    email = db.StringField()
    employer = db.StringField()
    firstname = db.StringField()
    gender = db.StringField()
    lastname = db.StringField(null=True)
    state = db.StringField(null=True)
