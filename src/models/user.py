import datetime
import logging
import secrets

# import requests
from flask import request
from flask_jwt_extended import create_access_token, get_jwt_identity
from flask_restplus import fields
from werkzeug.exceptions import Unauthorized, BadRequest

from src.models import db, bcrypt

_logger = logging.getLogger(__name__)


class User(db.Document):
    email = db.StringField(max_length=45, unique=True)
    name = db.StringField(max_length=45)
    status = db.StringField(max_length=45)
    password_hash = db.StringField(max_length=255)
    role = db.StringField(max_length=45, required=True, default='normal')
    last_login = db.DateTimeField(default=datetime.datetime.now)
    created_at = db.DateTimeField(default=datetime.datetime.now)
    updated_at = db.DateTimeField(default=datetime.datetime.now, onupdate=datetime.datetime.now)

    @property
    def password(self):
        raise AttributeError('password: write-only field')

    @password.setter
    def password(self, password):
        self.password_hash = bcrypt.generate_password_hash(password).decode('utf-8')

    def check_password(self, password):
        return bcrypt.check_password_hash(self.password_hash, password)

    def get_id(self):
        return self.id

    @property
    def is_authenticated(self):
        return True

    @staticmethod
    def get_logged_in_user(req=None, raise_on_error=True):
        if not req:
            req = request
        current_user = get_jwt_identity()
        if not current_user:
            raise Unauthorized("Unauthorized")
        user = User.objects(email=current_user['email']).first()
        return user

    @staticmethod
    def login_with_email(email, password):
        try:
            user = User.objects(email=email).first()
            if not user.password_hash:
                raise BadRequest("Account haven't set password")
            if user.check_password(password):
                access_token = create_access_token({
                    'email': user.email,
                    'role': user.role
                })
                return user, access_token
            else:
                raise Unauthorized('Email or Password not correct')
        except:
            raise Unauthorized('Email or Password not correct')


class UserSchema:
    user = {
        'id': fields.Integer(required=True, description='user id'),
        'email': fields.String(required=True, description='user email address'),
        'name': fields.String(required=False, description='user name'),
        'role': fields.String(required=False, description='user role (admin | moderator | viewer)',
                              enum=['viewer', 'admin', 'moderator', 'customer-care'])
        # 'password_hash': fields.String(required=False, description='user password hash'),
    }

    user_post = user.copy()
    user_post.pop('id', None)
    user_post.update({
        'password': fields.String(required=True, description='user password'),
    })

    user_put = user_post.copy()
    user_put.pop('password', None)
    user_put.pop('email', None)


class Token(db.Document):
    token = db.StringField(max_length=255)
    user_id = db.IntField()

    @staticmethod
    def delete_token(token):
        obj_token = Token.first(Token.token == token)
        if obj_token:
            obj_token.delete()

    @staticmethod
    def delete_token_by_user_id(user_id):
        Token.q(Token.user_id == user_id).delete()
