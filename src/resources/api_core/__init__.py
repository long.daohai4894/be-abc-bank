# coding=utf-8
import logging

from flask import Blueprint, url_for, current_app as app
from flask_restplus import Api

from src.resources.api_core.api_user import ns as user_ns
from src.resources.api_core.api_bank_account import ns as bank_account_ns

from src.extensions.exceptions import global_error_handler

_logger = logging.getLogger(__name__)

api_bp = Blueprint('api-bank-account', __name__, url_prefix='/api')

authorization = {
    'access-token': {
        'type': 'apiKey',
        'in': 'header',
        'name': 'Authorization'
    }
}


class MyApi(Api):
    @property
    def specs_url(self):
        scheme = app.config['URL_SCHEMA']
        return url_for(self.endpoint('specs'), _external=True, _scheme=scheme)


api = MyApi(
    app=api_bp,
    version='1.0',
    title='ABC Bank API',
    validate=False,
    swagget='2.0',
    authorizations=authorization
    # doc='' # disable Swagger UI
)

api.add_namespace(bank_account_ns, path='/bank-account')
api.add_namespace(user_ns, path='/user')
api.error_handlers[Exception] = global_error_handler


def init_api(app, **kwargs):
    """
    :param flask.Flask app: the app
    :param kwargs:
    :return:
    """
    app.register_blueprint(api_bp)
