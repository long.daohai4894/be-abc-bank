from flask import request
from flask_mongoengine import Pagination
from werkzeug.exceptions import BadRequest
from flask_restplus import Resource, fields
from mongoengine.queryset.visitor import Q

from src.extensions.namespace import Namespace
from src.helpers.login_manager import admin_required
from src.helpers.request_helper import RequestHelper
from src.helpers.response_helper import ResponseHelper
from src.models import BankAccount

ns = Namespace('bank_account', description='APIs for bank accounts')

_metadata = ns.model('METADATA', {
    'current_page': fields.Integer(),
    'page_size': fields.Integer(),
    'total_items': fields.Integer(),
    'next_page': fields.Integer(),
    'previous_page': fields.Integer(),
    'total_pages': fields.Integer()
})

_bank_account = ns.model('BANK_ACCOUNT', {
    'id': fields.String(description='ID'),
    'account_number': fields.String(description='Account Number'),
    'balance': fields.Integer(description='Balance'),
    'firstname': fields.String(description='firstname'),
    'lastname': fields.String(description='lastname'),
    'age': fields.Integer(description='age'),
    'gender': fields.String(description='gender'),
    'address': fields.String(description='address'),
    'employer': fields.String(description='employer'),
    'email': fields.String(description='email'),
    'city': fields.String(description='city'),
    'state': fields.String(description='state'),
})

_bank_account_post = ns.model('BANK_ACCOUNT_POST', {
    'account_number': fields.String(description='Account Number'),
    'balance': fields.Integer(description='Balance'),
    'firstname': fields.String(description='firstname'),
    'lastname': fields.String(description='lastname'),
    'age': fields.Integer(description='age'),
    'gender': fields.String(description='gender'),
    'address': fields.String(description='address'),
    'employer': fields.String(description='employer'),
    'email': fields.String(description='email'),
    'city': fields.String(description='city'),
    'state': fields.String(description='state'),
})

_bank_account_detail = ns.model('BANK_ACCOUNT_DETAIL', {
    'id': fields.String(description='ID'),
    'account_number': fields.String(description='Account Number'),
    'balance': fields.Integer(description='Balance'),
    'firstname': fields.String(description='firstname'),
    'lastname': fields.String(description='lastname'),
    'age': fields.Integer(description='age'),
    'gender': fields.String(description='gender'),
    'address': fields.String(description='address'),
    'employer': fields.String(description='employer'),
    'email': fields.String(description='email'),
    'city': fields.String(description='city'),
    'state': fields.String(description='state'),
})

_bank_account_put = ns.model('BANK_ACCOUNT_PUT', {
    'account_number': fields.String(description='Account Number'),
    'balance': fields.Integer(description='Balance'),
    'firstname': fields.String(description='firstname'),
    'lastname': fields.String(description='lastname'),
    'age': fields.Integer(description='age'),
    'gender': fields.String(description='gender'),
    'address': fields.String(description='address'),
    'employer': fields.String(description='employer'),
    'email': fields.String(description='email'),
    'city': fields.String(description='city'),
    'state': fields.String(description='state'),
})


@ns.route('', methods=['GET', 'POST'])
class BankAccounts(Resource):
    @ns.doc(security='access-token')
    @admin_required('admin', 'normal')
    @ns.expect(RequestHelper.get_bank_account_list_filters(), validate=True)
    @ns.marshal_with(_bank_account, as_list=True, metadata=_metadata)
    def get(self):
        """Api GET danh sách bank account"""
        args = RequestHelper.get_bank_account_list_filters().parse_args()
        page = args['page']
        page_size = args['pageSize']
        account_number = args.get('account_number')
        address = args.get('address')
        age = args.get('age')
        city = args.get('city')
        email = args.get('email')
        employer = args.get('employer')
        firstname = args.get('firstname')
        lastname = args.get('lastname')
        gender = args.get('gender')
        state = args.get('state')
        all_fields = args.get('all_fields')

        _query = BankAccount.objects

        if account_number:
            _query = _query.filter(account_number=account_number)

        if address:
            _query = _query.filter(address__contains=address)

        if age:
            _query = _query.filter(age=age)

        if city:
            _query = _query.filter(city__contains=city)

        if email:
            _query = _query.filter(email__contains=email)

        if employer:
            _query = _query.filter(employer__contains=employer)

        if firstname:
            _query = _query.filter(firstname__contains=firstname)

        if lastname:
            _query = _query.filter(lastname__contains=lastname)

        if gender:
            _query = _query.filter(gender=gender)

        if state:
            _query = _query.filter(state__contains=state)

        if all_fields:
            _query = _query.filter(
                Q(address__contains=all_fields) | Q(employer__contains=all_fields) | Q(
                    firstname__contains=all_fields) | Q(lastname__contains=all_fields) | Q(state__contains=all_fields))

        _paginator = Pagination(_query, page, page_size)
        bank_accounts = _paginator.items
        total_items = _paginator.total
        return {
            'data': bank_accounts,
            'metadata': ResponseHelper.pagination_meta(page, page_size, total_items)}

    @ns.doc(security='access-token')
    @admin_required('admin')
    @ns.expect(_bank_account_post, validate=True)
    @ns.marshal_with(_bank_account_detail)
    def post(self):
        """Api POST bank account"""
        data = request.json
        bank_account = BankAccount(**{
            'account_number': data['account_number'] if data['account_number'] else None,
            'balance': data['balance'] if data['balance'] else None,
            'firstname': data['firstname'] if data['firstname'] else None,
            'lastname': data['lastname'] if data['lastname'] else None,
            'age': data['age'] if data['age'] else None,
            'gender': data['gender'] if data['gender'] else None,
            'address': data['address'] if data['address'] else None,
            'employer': data['employer'] if data['employer'] else None,
            'email': data['email'] if data['email'] else None,
            'city': data['city'] if data['city'] else None,
            'state': data['state'] if data['state'] else None,
        })
        bank_account.save()
        return bank_account, 200


@ns.route('/<string:bank_account_id>', methods=['GET', 'PUT'])
class BankAccountByIdApi(Resource):
    @ns.doc(security='access-token')
    @admin_required('admin', 'normal')
    @ns.marshal_with(_bank_account_detail)
    def get(self, bank_account_id):
        """Api GET detail bank account"""
        try:
            bank_account = BankAccount.objects.get(pk=bank_account_id)
        except:
            raise BadRequest("Bank Account is not exist")
        return bank_account, 200

    @ns.doc(security='access-token')
    @admin_required('admin')
    @ns.expect(_bank_account_post, validate=True)
    @ns.marshal_with(_bank_account_detail)
    def put(self, bank_account_id):
        """Api PUT bank account"""
        try:
            bank_account = BankAccount.objects.get(pk=bank_account_id)
            bank_account.update(**request.json)
            bank_account.reload()
        except Exception as e:
            raise BadRequest(e)
        return bank_account, 200
