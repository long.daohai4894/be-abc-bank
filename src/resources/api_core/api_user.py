import logging
from flask import request
import flask_restplus as _fr
from werkzeug.exceptions import BadRequest
from flask_restplus import Resource, reqparse
from src.extensions.namespace import Namespace

from src.extensions.response_wrapper import wrap_response
from src.helpers.login_manager import admin_required
from src.models import bcrypt
from src.models.user import User

_logger = logging.getLogger(__name__)

ns = Namespace('user', description='User operations')


@ns.route('/login')
class LoginApi(Resource):
    """
    Api for user user management
    """
    LOGIN_INFO = ns.model('LOGIN_INFO', {
        'email': _fr.fields.String(),
        'password': _fr.fields.String()
    })

    @ns.expect(LOGIN_INFO)
    def post(self):
        parser = reqparse.RequestParser()
        parser.add_argument('email', type=str, location='json')
        parser.add_argument('password', type=str, location='json')
        params = parser.parse_args()

        email = params['email']
        password = params['password']

        if email and password:
            user, jwt_token = User.login_with_email(email, password)
            return wrap_response(http_code=200, message='OK', data={
                'email': user.email,
                'role': user.role,
                'token': jwt_token
            })
        raise BadRequest("Invalid params")


@ns.route('/info', methods=['GET', 'POST'])
class UserApi(Resource):
    _user_post = ns.model('USER_POST', {
        'email': _fr.fields.String(description='email'),
        'name': _fr.fields.String(description='name'),
        'status': _fr.fields.String(description='status'),
        'password': _fr.fields.String(description='password'),
        'role': _fr.fields.String(description='role'),
    })

    _user_detail = ns.model('USER_DETAIL', {
        'email': _fr.fields.String(description='email'),
        'name': _fr.fields.String(description='name'),
        'role': _fr.fields.String(description='role'),
    })

    @ns.doc(security='access-token')
    @admin_required('admin', 'normal')
    @ns.marshal_with(_user_detail)
    def get(self):
        user = User.get_logged_in_user()
        user_data = {
            'email': user.email,
            'name': user.name,
            'role': user.role
        }
        return user_data

    @ns.doc(security='access-token')
    @admin_required('admin')
    @ns.expect(_user_post, validate=True)
    def post(self):
        """Api POST User account"""
        data = request.json
        user = User(**{
            'email': data['email'] if data['email'] else None,
            'name': data['name'] if data['name'] else None,
            'status': data['status'] if data['status'] else None,
            'password_hash': bcrypt.generate_password_hash(data['password']).decode('utf-8'),
            'role': data['role'] if data['role'] else None
        })
        user.save()
        return {
                   "code": 200,
                   "success": True,
                   "message": ""
               }, 200
