from dotenv import load_dotenv, find_dotenv
load_dotenv(find_dotenv())

import logging
from main import app as application

_logger = logging.getLogger(__name__)

if __name__ == "__main__":
    application.run()
